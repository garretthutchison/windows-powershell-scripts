param(
    [String][ValidateSet('Detect','InstallService','RemoveService')]$RunType = "InstallService"
)

# Prepare a Windows Update service manager
$ServiceManager = New-Object -ComObject "Microsoft.Update.ServiceManager"

$ServiceID = "7971f918-a847-4430-9279-4a52d1efe18d"

# 
switch ($RunType)
{
    "Detect"
    {
        $found = $false
        foreach($service in $ServiceManager.Services)
        {
            if($service.ServiceID -eq $ServiceID)
            {
                $found = $true
            }
        }

        if($found)
        {
            "Found Service"
        }
    }
    "InstallService"
    {
        $ServiceManager.ClientApplicationID = "My App"
        try
        {
            $NewService = $ServiceManager.AddService2($ServiceID,7,"")
        }
        catch
        {
            Write-Warning "Failed to register service"
            Write-Warning $_.Exception.Message
            Exit 1
        }
        if($NewService.IsPendingRegistrationWithAU)
        {
            Write-Verbose "Needs to reboot"
            Exit 3010
        }
        else
        {
            Exit 0
        }
    }
    "RemoveService"
    {
        try
        {
            $ServiceManager.RemoveService($ServiceID)
        }
        catch
        {
            if($_.Exception.ErrorCode -eq -2145091564)
            {
                Write-Verbose "The service doesn't exist, so exit successfully"
                Exit 0
            }
            else
            {
                Write-Warning "Failed to remove service"
                Write-Warning $_.Exception.Message
                Exit 1
            }
        }
    }
    default
    {
        Write-Warning "No RunType set. Exiting"
        Exit 1
    }
}